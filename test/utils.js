import checkPropType from "check-prop-types";
import { configureStore } from "@reduxjs/toolkit";
import toastReducer from "../src/redux/toastReducer";
// import { middlewares } from "../src/configureStore";

export const storeFactory = (initialState) => {
  return configureStore({
    reducer: {
      toast: toastReducer,
    },
    initialState,
  });
};

export const checkProps = (component, conformingProps) => {
  const propError = checkPropType(
    component.propTypes,
    conformingProps,
    "prop",
    component.name
  );
  expect(propError).toBeUndefined();
};

export const findByTestAttr = (wrapper, val) =>
  wrapper.find(`[data-test='${val}']`).first();
