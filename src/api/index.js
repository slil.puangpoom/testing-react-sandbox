export const getAllProduct = () => {
  return new Promise((resolve) =>
    resolve([
      {
        name: "iphone",
        stock: 100,
      },
      {
        name: "samsung",
        stock: 55,
      },
    ])
  );
};
