import { Container, Row, Col } from "react-bootstrap";
import Counter from "./Counter";
import List from "./List";

function App() {
  return (
    <Container className="mt-5">
      <Row>
        <Col>
          <Counter></Counter>
        </Col>
        <Col>
          <List></List>
        </Col>
      </Row>
    </Container>
  );
}

export default App;
