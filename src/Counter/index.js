import React, { useState } from "react";
import { Container, Button, Col, Row, Badge } from "react-bootstrap";

function Counter() {
  const [count, setCount] = useState(0);

  return (
    <Container>
      <Row>
        <Col xs={4}>
          <Button
            onClick={() => {
              setCount((prev) => prev - 1);
            }}
          >
            <h2>-</h2>
          </Button>
        </Col>
        <Col xs={4}>
          <h2>
            <Badge>
              <span data-testid="display-counter">{count}</span>
            </Badge>
          </h2>
        </Col>
        <Col xs={4}>
          <Button
            onClick={() => {
              setCount((prev) => prev + 1);
            }}
          >
            <h2>+</h2>
          </Button>
        </Col>
      </Row>
    </Container>
  );
}

export default Counter;
