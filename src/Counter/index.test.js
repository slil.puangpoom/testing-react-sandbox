import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import Counter from "./index";

const defaultProps = {};

const setUp = (props = {}) => {
  const setProps = { ...defaultProps, ...props };
  return render(<Counter {...setProps}></Counter>);
};

describe("render all component", () => {
  beforeEach(() => {
    setUp();
  });
  it("should render display count start at 0", () => {
    const displayCount = screen.getByTestId("display-counter");
    expect(displayCount).toBeInTheDocument();
    expect(displayCount).toHaveTextContent("0");
  });

  it("should render increment button", () => {
    const incrementBtn = screen.getByText("+");
    expect(incrementBtn).toBeInTheDocument();
  });

  test("should render decrement button", () => {
    const decrementalBtn = screen.getByText(/-/i);
    expect(decrementalBtn).toBeInTheDocument();
  });
});

describe("click decrease increase button", () => {
  beforeEach(() => {
    setUp();
  });
  it("should display 1 when click increment button", () => {
    const incrementBtn = screen.getByText("+");
    fireEvent.click(incrementBtn);

    const displayCount = screen.getByTestId("display-counter");
    expect(displayCount).toHaveTextContent("1");
  });

  it("should display -1 when click decrement button", () => {
    const decrementalBtn = screen.getByText(/-/i);
    fireEvent.click(decrementalBtn);

    const displayCount = screen.getByTestId("display-counter");
    expect(displayCount).toHaveTextContent("-1");
  });

  it("should display 0 when click increment then decrement button", () => {
    const incrementBtn = screen.getByText("+");
    fireEvent.click(incrementBtn);

    const decrementalBtn = screen.getByText(/-/i);
    fireEvent.click(decrementalBtn);

    const displayCount = screen.getByTestId("display-counter");
    expect(displayCount).toHaveTextContent("0");
  });
});
