import React from "react";
import { render, screen, waitFor } from "@testing-library/react";
import List from "./index";
import { getAllProduct } from "../api";

jest.mock("../api", () => ({
  __esModule: true, // this property makes it work
  getAllProduct: jest.fn(),
}));

const defaultProps = {};

describe("render all component", () => {
  it("should render number row of table data depend on get data", async () => {
    getAllProduct.mockReturnValueOnce(
      new Promise((resolve) =>
        resolve([
          {
            name: "iphone",
            stock: 100,
          },
          {
            name: "samsung",
            stock: 55,
          },
        ])
      )
    );

    render(<List></List>);

    expect(getAllProduct).toHaveBeenCalledTimes(1);

    await waitFor(() =>
      expect(screen.getAllByTestId("table-data")).toHaveLength(2)
    );
  });

  it("should render number row of table data depend on get data (use find method)", async () => {
    getAllProduct.mockReturnValueOnce(
      new Promise((resolve) =>
        resolve([
          {
            name: "iphone",
            stock: 100,
          },
          {
            name: "samsung",
            stock: 55,
          },
        ])
      )
    );

    render(<List></List>);

    const dataTable = await screen.findAllByTestId("table-data");
    expect(dataTable).toHaveLength(2);
  });
});
