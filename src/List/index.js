import React, { useEffect, useState } from "react";
import { Container, Button, Col, Row } from "react-bootstrap";
import { getAllProduct } from "../api";

function List() {
  const [product, setProduct] = useState([]);
  const getInitialData = async () => {
    const data = await getAllProduct();
    setProduct(data);
  };
  useEffect(() => {
    getInitialData();
  }, []);

  return (
    <Container>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Stock</th>
          </tr>
        </thead>
        <tbody data-testid="list">
          {product?.map((item) => (
            <tr key={item?.stock} data-testid="table-data">
              <th>{item?.name}</th>
              <th>{item?.stock}</th>
            </tr>
          ))}
        </tbody>
      </table>
    </Container>
  );
}

export default List;
