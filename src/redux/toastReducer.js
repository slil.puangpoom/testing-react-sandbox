import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  label: "",
};

export const toastSlice = createSlice({
  name: "toast",
  initialState,
  reducers: {
    successMessage: (state) => {
      state.label = "SUCCESS";
    },
    failMessage: (state) => {
      state.label = "FAIL";
    },
  },
});

// Action creators are generated for each case reducer function
export const { successMessage, failMessage } = toastSlice.actions;

export default toastSlice.reducer;
