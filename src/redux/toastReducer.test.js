import reducer, { successMessage, failMessage } from "./toastReducer";

const initialState = {
  label: "",
};

test("should return the initial state", () => {
  expect(reducer(initialState, {})).toEqual({
    label: "",
  });
});

test("should set label to success", () => {
  expect(reducer(initialState, successMessage())).toEqual({
    label: "SUCCESS",
  });
});

test("should set label to fail", () => {
  expect(reducer(initialState, failMessage())).toEqual({
    label: "FAIL",
  });
});
